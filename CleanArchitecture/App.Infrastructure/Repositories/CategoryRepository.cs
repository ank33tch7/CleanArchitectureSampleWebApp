﻿using App.Application.Command.Category;
using App.Application.Command.Product.CreateProduct;
using App.Application.Interfaces.IRepositories;
using App.Application.Services;
using App.Domain.Entities;
using App.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ICurrentUserService _currentUser;
        public CategoryRepository(ApplicationDbContext context, ICurrentUserService currentUser) : base(context)
        {
            _context= context;
            _currentUser= currentUser;
        }
        public async Task<IEnumerable<CategoryLists>> GetAllCategories()
        {
            return await _context.Categories
                                    .OrderByDescending(x => x.CreatedDate)
                                    .Where(x => x.IsDeleted == false)
                                    .Select(x => new CategoryLists
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Description = x.Description,
                                        CreatedBy = _context.Users.FirstOrDefault(y => y.Id == x.CreatedBy).FullName,
                                        
                                    }).ToListAsync();
        }

        public async Task<IEnumerable<CategoryLists>> CategoryListForDropDown()
        {
            return await _context.Categories
                                .Where(x => x.IsDeleted == false)
                                .OrderBy(x => x.Name)
                                .Select(x =>new CategoryLists
                                {
                                    Id = x.Id,
                                    Name = x.Name
                                }).ToListAsync();
        }

        public async Task CreateCategory(CreateCategoryCommand command)
        {
            var category = new Category
            {
                Name = command.Name,
                Description= command.Description,
                Image = command.Image
            };
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateCategory(UpdateCategoryCommand command)
        {
            var updateCategory = await _context.Categories.FindAsync(command.Id);
            if (updateCategory != null)
            {
                updateCategory.Name = command.Name;
                updateCategory.Description = command.Description;
                updateCategory.Image = command.Image;
            }
            await _context.SaveChangesAsync();
        }
        public async Task DeleteCategory(Guid id)
        {
            var categoryDetails = await _context.Categories.FindAsync(id);
            categoryDetails.IsDeleted = true;
            categoryDetails.DeletedBy = _currentUser.UserId;
            categoryDetails.DeletedDate = DateTime.UtcNow;

            await _context.SaveChangesAsync();
        }

    }
}
