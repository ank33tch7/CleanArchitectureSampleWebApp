﻿using Microsoft.Extensions.DependencyInjection;

namespace App.Application
{
    public static class ApplicationDependencyInjection
    {
        public static IServiceCollection AddApplicationDependencyInjection(this IServiceCollection services)
        {
            return services;
        }
    }
}
