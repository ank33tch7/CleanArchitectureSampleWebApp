﻿using App.Application.Command.Category;
using App.Application.Command.Product.CreateProduct;
using App.Application.Command.Product.UpdateProduct;
using App.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Application.Interfaces.IRepositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        Task CreateCategory(CreateCategoryCommand command);
        Task UpdateCategory(UpdateCategoryCommand command);
        Task DeleteCategory(Guid id);
        Task<IEnumerable<CategoryLists>> GetAllCategories();
        Task<IEnumerable<CategoryLists>> CategoryListForDropDown();
    }
}
