﻿using App.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.WebUI.API_Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderDetailController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderDetailController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet("get-all-order-details")]
        public async Task<ActionResult> GetAllOrderDetails()
        {
            var orderDetails = await _unitOfWork.OrderDetails
                                                .GetAll()
                                                .ToListAsync();
            return Ok(orderDetails);
        } 
    }
}
