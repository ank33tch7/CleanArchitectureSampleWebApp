﻿using App.Application.Command.ApplicationUser;
using App.Application.Interfaces;
using App.Application.Services;
using App.Domain.Entities;
using App.Domain.Enums;
using App.Infrastructure.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;

namespace App.WebUI.API_Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUserService _currentUserService;
        public AccountController(UserManager<ApplicationUser> userManager,
                                 SignInManager<ApplicationUser> signInManager,
                                 IJwtTokenGenerator jwtTokenGenerator,
                                 IUnitOfWork unitOfWork,
                                 ICurrentUserService currentUserService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtTokenGenerator = jwtTokenGenerator;
            _unitOfWork = unitOfWork;
            _currentUserService = currentUserService;
        }

        [HttpPost("create-user")]
        [AllowAnonymous]
        public async Task<ActionResult<string>> CreateUser([FromBody] CreateApplicationUserCommand command)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FullName = command.FullName,
                Email = command.Email,
                Phone = command.PhoneNumber,
                Gender = command.Gender,
                DateOfBirth = command.DateOfBirth,
                Address = command.Address
            };

            if (command.UserType is UserType.User)
            {
                _unitOfWork.Customers.Add(customer);
                await _unitOfWork.SaveChangesAsync();
            }

            var user = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                FullName = command.FullName,
                UserName = command.UserName,
                Email = command.Email,
                PhoneNumber = command.PhoneNumber,
                UserType = command.UserType,
                IsActive = command.IsActive,
                Gender = command.Gender,
                DateOfBirth = command.DateOfBirth,
                Image = command.Image,
                Address = command.Address,
                CustomerId = command.UserType is UserType.User ? customer.Id : null,
            };

            IdentityResult result = await _userManager.CreateAsync(user,command.Password);

            if (!result.Succeeded)
                return BadRequest();

            return Ok(user.Id);
        }

        [HttpPost("authenticate-user")]
        [AllowAnonymous]
        public async Task<ActionResult<TokenResponse>> AuthenticateUser([FromBody] AuthenticateRequest request)
        {
            var identityUser = await _userManager.FindByNameAsync(request.UserName);

            if (identityUser is null && identityUser.IsActive is true)
                return Unauthorized();

            if (identityUser.UserType is not UserType.Admin)
                return Unauthorized();

            var result = await _signInManager.CheckPasswordSignInAsync(identityUser, request.Password,lockoutOnFailure: false);

            if (!result.Succeeded)
                return Unauthorized();

            var user = new AppUser
            {
                Id = identityUser.Id,
                FullName = identityUser.FullName,
                UserName = identityUser.UserName,
                Email = identityUser.Email,
                UserType = identityUser.UserType,
                CustomerId = identityUser.CustomerId.ToString(),
                Address = identityUser.Address,
                PhoneNumber = identityUser.PhoneNumber,
            };

            var securitToken = _jwtTokenGenerator.GetToken(user);

            var tokenResult = new TokenResponse
            {
                Token = new JwtSecurityTokenHandler().WriteToken(securitToken),
                ExpiryDate = securitToken.ValidTo.ToString(),
            };

            return Ok(tokenResult);
        }

        [HttpPost("change-password")]
        public async Task<ActionResult> ChangePassword([FromBody] ChangePasswordRequest request)
        {
            var userId = _currentUserService?.UserId;
            var identityUser = await _userManager.FindByIdAsync(userId);

            if (identityUser == null && identityUser.IsActive is true && identityUser.UserType == UserType.Admin)
                return Unauthorized();

            var result = await _userManager.ChangePasswordAsync(identityUser,request.OldPassword,request.NewPassword);

            if(!result.Succeeded)
                return BadRequest(result.Errors);

            return Ok();
        }

    }
    public class TokenResponse
    {
        public string Token { get; set; }
        public string ExpiryDate { get; set; }
    }   
}
