﻿using App.Application.Command.Category;
using App.Application.Command.Product.CreateProduct;
using App.Application.Interfaces;
using App.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static System.Net.Mime.MediaTypeNames;

namespace App.WebUI.API_Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoryController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet("get-all-category")]
        public async Task<ActionResult<List<CategoryLists>>> GetAllCategories()
        {
            var categories = await _unitOfWork.Categories.GetAllCategories();
            return Ok(categories);
        }

        [HttpGet("get-category-for-dropdown")]
        public async Task<ActionResult<List<CategoryListForDropDown>>> GetCategoryForDropDown()
        {
            var categoryListForDropDown = await _unitOfWork.Categories.CategoryListForDropDown();
            return Ok(categoryListForDropDown);
        }

        [HttpPost("create-category")]
        public async Task<IActionResult> CreateCategory([FromBody] CreateCategoryCommand command)
        {
            await _unitOfWork.Categories.CreateCategory(command);
            return Ok();
        }
        [HttpPut("update-category")]
        public async Task<IActionResult> UpdateCategory([FromBody] UpdateCategoryCommand command)
        {
            await _unitOfWork.Categories.UpdateCategory(command);
            return Ok();
        }
        [HttpPut("delete-category/{id}")]
        public async Task<IActionResult> DeleteCategory([FromRoute] Guid id)
        {
            await _unitOfWork.Categories.DeleteCategory(id);
            return Ok();
        }

        
    }
}
