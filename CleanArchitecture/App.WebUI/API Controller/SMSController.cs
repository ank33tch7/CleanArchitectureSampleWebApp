﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace App.WebUI.API_Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class SMSController : ControllerBase
    {
        public SMSController()
        {

        }
        [HttpGet("send-sms")]
        [AllowAnonymous]
        public async Task<ActionResult> SendSMS()
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("https://nexmo-nexmo-sms-verify-v1.p.rapidapi.com/send-verification-code?phoneNumber=+9779862699637&brand=%ankit%3E"),
                Headers =
                    {
                        { "X-RapidAPI-Key", "b94b7272ddmshb852328ecd8addap171341jsn67ded7d5212e" },
                        { "X-RapidAPI-Host", "nexmo-nexmo-sms-verify-v1.p.rapidapi.com" },
                    },
            };
            using (var response = await client.SendAsync(request))
            {
                //response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                return Ok(body);
            }
            //        var client = new HttpClient();
            //        var request = new HttpRequestMessage
            //        {
            //            Method = HttpMethod.Post,
            //            RequestUri = new Uri("https://telesign-telesign-send-sms-verification-code-v1.p.rapidapi.com/sms-verification-code?phoneNumber=9862699637&verifyCode=Test"),
            //            Headers =
            //{
            //    { "X-RapidAPI-Key", "5cfa490c4dmsh2d3a3fb88bbf77cp151d87jsnc62999aa8abf" },
            //    { "X-RapidAPI-Host", "telesign-telesign-send-sms-verification-code-v1.p.rapidapi.com" },
            //},
            //        };
            //        using (var response = await client.SendAsync(request))
            //        {
            //            response.EnsureSuccessStatusCode();
            //            var body = await response.Content.ReadAsStringAsync();
            //            return Ok(body);
            //        }
            //using(var _client = new HttpClient())
            //{
            //    var request = new HttpRequestMessage
            //    {
            //        Method = HttpMethod.Post,
            //        RequestUri = new Uri("https://sms77io.p.rapidapi.com/sms"),
            //        Headers =
            //        {
            //            { "X-RapidAPI-Key", "5cfa490c4dmsh2d3a3fb88bbf77cp151d87jsnc62999aa8abf" },
            //            { "X-RapidAPI-Host", "sms77io.p.rapidapi.com" },
            //        },
            //        Content = new FormUrlEncodedContent(new Dictionary<string, string>
            //        {
            //            { "to", "+9779862699637" },
            //            { "p", "5cfa490c4dmsh2d3a3fb88bbf77cp151d87jsnc62999aa8abf" },
            //            { "text", "Hello Test Message" },
            //        }),
            //    };
            //    using (var response = await _client.SendAsync(request))
            //    {
            //        response.EnsureSuccessStatusCode();
            //        var body = await response.Content.ReadAsStringAsync();
            //        return Ok(body);
            //    }
            //}
        }
    }
}
