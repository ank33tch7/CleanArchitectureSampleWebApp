import { Component, OnInit } from '@angular/core';
import { SMSService } from 'src/app/services/web-api-clients';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _smsService: SMSService) { }

  ngOnInit(): void {
    this._smsService.sendSMS().subscribe(res =>{

    });
  }

}
