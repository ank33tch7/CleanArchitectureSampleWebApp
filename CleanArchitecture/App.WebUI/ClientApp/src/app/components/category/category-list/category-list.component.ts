import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from 'src/app/services/web-api-clients';
import {
  ConfirmBoxInitializer,
  DialogLayoutDisplay,
  DisappearanceAnimation,
  AppearanceAnimation
} from '@costlydeveloper/ngx-awesome-popup';
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  //for ag-grid
  categoryData: any;
  categoryColumnDefs: any;
  domLayout : any;
  gridApi: any;
  gridColumnApi: any;
  quickSearchValue: any;

  //for popupmodel
  createCategoryPopUpModal: boolean = false;
  updateCategoryPopUpModal: boolean = false;

  updateCategoryDetails: any;
  constructor(private _categoryService: CategoryService,
              private _toastrService: ToastrService) {
    this.domLayout = "autoHeight";
   }

  ngOnInit(): void {
    this.loadCategoryDetails();
    this.categoryColumnDefs = [
      { headerName: "S.N", valueGetter: 'node.rowIndex+1', width: 40, resizable: true },
      { headerName: "Name", field: 'name', sortable: true, resizable: true, width: 100 },
      { headerName: "Description", field: 'description', sortable: true, resizable: true, width: 100 },
      { headerName: "Created By", field: 'createdBy', sortable: true, resizable: true, width: 100},
      { headerName: "Actions", field: 'action', cellRenderer: this.actions(),pinned: 'right', resizable: true, width: 100},
    ];
  }

  public actions() {
    return function(params :  any){
      return `<button type="button" data-action-type="Edit" class="btn ag-btn" 
              data-toggle="tooltip" data-placement="bottom" title = "Edit" >  Edit </button > &nbsp; &nbsp;
              <button type="button" data-action-type="Remove" class="btn ag-btn" 
              data-toggle="tooltip" data-placement="bottom" title = "Remove" >  Remove </button >`;
    }
       
  }

  loadCategoryDetails() : void {
    this._categoryService.getAllCategories().subscribe(categoryData => {
      this.categoryData = categoryData;
    },err =>{
      this._toastrService.error('Something went wrong.', 'Error!');
    });
  }
  onRowClicked(e: any){
    if (e.event.target) {
      let data = e.data;
      let actionType = e.event.target.getAttribute("data-action-type");

      switch (actionType) {
        case "Edit": {
          this.updateCategoryDetails = data;
          this.updateCategoryPopUpModal = true;
          break;
        }
        case "Remove":
          return this.onOpenDialog(data);
      }
    }
  }

  onFilterChanged(e: any){
    e.api.refreshCells();
  }

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.quickSearchValue);
    
  }
  onModelUpdated(){
    setTimeout(() => { this.gridColumnApi.autoSizeAllColumns() });
  }


  onGridReady(params : any){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onCreateCategoryPopUp(){
    this.createCategoryPopUpModal = true;    
  }

  close(){
    this.createCategoryPopUpModal = false;
    this.updateCategoryPopUpModal = false;
  }

  callbackCategory(){
    this.close();
    this.loadCategoryDetails();
  }

  onOpenDialog(row: any){
    const newConfirmBox = new ConfirmBoxInitializer();

    newConfirmBox.setTitle('Warning!!!');
    newConfirmBox.setMessage('Are you sure you want to remove this category?');
    newConfirmBox.setButtonLabels('YES', 'NO');
    // Choose layout color type
    newConfirmBox.setConfig({
    layoutType: DialogLayoutDisplay.WARNING,// SUCCESS | INFO | NONE | DANGER | WARNING
    animationIn: AppearanceAnimation.BOUNCE_IN, // BOUNCE_IN | SWING | ZOOM_IN | ZOOM_IN_ROTATE | ELASTIC | JELLO | FADE_IN | SLIDE_IN_UP | SLIDE_IN_DOWN | SLIDE_IN_LEFT | SLIDE_IN_RIGHT | NONE
    animationOut: DisappearanceAnimation.BOUNCE_OUT, // BOUNCE_OUT | ZOOM_OUT | ZOOM_OUT_WIND | ZOOM_OUT_ROTATE | FLIP_OUT | SLIDE_OUT_UP | SLIDE_OUT_DOWN | SLIDE_OUT_LEFT | SLIDE_OUT_RIGHT | NONE
    });

    // Simply open the popup
    newConfirmBox.openConfirmBox$().subscribe(res =>{
      if(res.clickedButtonID ==='yes'){
        this._categoryService.deleteCategory(row.id).subscribe(res =>{
          this._toastrService.success('Product removed successfully.','Success!');
          this.loadCategoryDetails();
        },err =>{
          this._toastrService.error('Something went wrong! Please try again...','Error!');
        });
      }
    });
  }

}
