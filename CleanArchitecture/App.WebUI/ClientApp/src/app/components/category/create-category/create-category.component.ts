import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from 'src/app/services/web-api-clients';

@Component({
  selector: 'create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {
@Output('callbackCategory') callback = new EventEmitter<object>();
  //for reactive form
  createCategoryForm: any;
  submitted: boolean = false;

  constructor(private _formBuilder: FormBuilder, 
              private _categoryService: CategoryService,
              private _toastrService: ToastrService) { }

  ngOnInit(): void {
    this.createCategoryForm = this._formBuilder.group({
      name: ['',Validators.required],
      description: ['',Validators.required],
      image: ['']
    });
  }

  get getFormControl(){
    return this.createCategoryForm.controls;
  }

  onSubmitCreateCategory():void {
    this.submitted = true;
    if(this.createCategoryForm.invalid) return; 

    this._categoryService.createCategory(this.createCategoryForm.value).subscribe(res =>{
      this._toastrService.success('Category created successfully','Success!');
      this.callback.emit();
    },err=>{
      this._toastrService.error('Something went wrong.', 'Error!');
    });

  }

}
