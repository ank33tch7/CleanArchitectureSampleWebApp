import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from 'src/app/services/web-api-clients';

@Component({
  selector: 'update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {
  @Input('update-category-details') updateCategoryDetails : any;
  @Output('callbackCategory') callback = new EventEmitter<object>();
  // for reactive form
  updateCategoryForm: any;
  submitted : boolean = false;

  constructor(private _formBuilder: FormBuilder, 
              private _categoryService: CategoryService,
              private _toastrService: ToastrService) { }

  ngOnInit(): void {
    this.updateCategoryForm = this._formBuilder.group({
      id : [null],
      name: ['',Validators.required],
      description: ['',Validators.required],
      image: ['']
    });
    this.updateCategoryForm.patchValue(this.updateCategoryDetails);
  }

  get getFormControl(){
    return this.updateCategoryForm.controls;
  }
  onSubmitUpdateCategory(){
    this.submitted = true;
    if(this.updateCategoryForm.invalid) return;
    this._categoryService.updateCategory(this.updateCategoryForm.value).subscribe(res =>{
      this._toastrService.success("Category updated successfully!","Success!");
      this.callback.emit();
    },err =>{
      this._toastrService.error("Category updated failed!","Error!");
    });
  }

}
