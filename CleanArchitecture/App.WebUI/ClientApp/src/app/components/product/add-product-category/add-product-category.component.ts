import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'add-product-category',
  templateUrl: './add-product-category.component.html',
  styleUrls: ['./add-product-category.component.css']
})
export class AddProductCategoryComponent implements OnInit {

  // reactive form
  productCategoryForm : any;
  submitted: boolean = false;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.productCategoryForm = this._formBuilder.group({

    });
  }

}
