import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { CategoryService, ProductService } from "src/app/services/web-api-clients";

@Component({
    selector: 'create-product',
    templateUrl: 'create-product.component.html'
})

export class CreateProductComponent implements OnInit{
@Output('callbackProduct') callback = new EventEmitter<object>();

    // for reactive form
    submitted : boolean = false;
    createProductForm : any;

    categoryListDropdown : any;
    listOfCategories: any;

    categoryId : any[] = [];
    constructor(private _formBuilder : FormBuilder,
                private _productService: ProductService,
                private _toastrService: ToastrService,
                private _categoryService: CategoryService){}

    ngOnInit(): void {
        this.getCategoryListForDropdown();
        this.createProductForm = this._formBuilder.group({
            name: ['',Validators.required],
            description: ['',Validators.required],
            unitPrice: ['',Validators.required],
            sellingUnitPrice: ['',Validators.required],
            quantity: ['',Validators.required],
            categoryId : ['',Validators.required],
            image: [''],
        });
    }

    get getFormControl(){
        return this.createProductForm.controls;
    }

    onSubmitCreateProduct() : void {
        this.submitted = true;
        if(this.createProductForm.invalid) return;

        let jsonRequest: any = {
            name : this.createProductForm.value.name,
            description : this.createProductForm.value.description,
            unitPrice : this.createProductForm.value.unitPrice,
            sellingUnitPrice : this.createProductForm.value.sellingUnitPrice,
            quantity : this.createProductForm.value.quantity,
            image : this.createProductForm.value.image,
            categories: this.categoryId
        };
        this._productService.createProduct(jsonRequest).subscribe(res =>{
            this._toastrService.success('Product created successfully.','Success!');
            this.callback.emit();
        },err =>{
            this._toastrService.error("Some thing went wrong.",'Error!');
        });
    }

    getCategoryListForDropdown() : void {
        this._categoryService.getCategoryForDropDown().subscribe(res =>{
            this.categoryListDropdown = res;
        });
    }

    onChangeCategoryDropDown(e: any): void{
        this.categoryId = [];
        let jsonObj;
        for(let i=0;i<e.length;i++){
            jsonObj = {
                categoryId : e[i].id
            };
        }
        this.categoryId.push(jsonObj);
    }
}
