import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth-service/auth-service.service';
import { AccountService } from 'src/app/services/web-api-clients';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm : any;
  submitted: boolean = false;
  constructor(private _formBuilder: FormBuilder,
            private _accountService: AccountService,
            private _toastrService: ToastrService,
            private _authService: AuthService) { }

  ngOnInit(): void {
    this.changePasswordForm = this._formBuilder.group({
      oldPassword : ["",Validators.required],
      newPassword: ["",Validators.required]
    });
  }

  get getFormControl(){
    return this.changePasswordForm.controls;
   }
  
   onSubmitChangePassword() : void {
    this.submitted = true;
    if(this.changePasswordForm.invalid) return;
     
    this._accountService.changePassword(this.changePasswordForm.value).subscribe(res =>{
      this._toastrService.success("Password changed successfully","Success!");
      this._authService.logout();
    },err =>{
      this._toastrService.error("Password changed failed","Error!");
      
    });
  


   }

}
