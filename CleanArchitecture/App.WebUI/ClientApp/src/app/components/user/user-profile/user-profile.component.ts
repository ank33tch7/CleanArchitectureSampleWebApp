import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth-service/auth-service.service';
import { HeaderComponent } from '../../shared/header/header.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private _authService: AuthService) { }

  ngOnInit(): void {

   
  }

  getFullname() : string {
    return this._authService?.userInfo?.fullName ?? "";
  }
  getUsername() : string{
    return this._authService?.userInfo?.userName ?? "";
  }
  getEmail() : string {
    return this._authService?.userInfo?.email ?? "";
  }
  getUserType() : string {
    return this._authService?.userInfo?.type ?? "";
  }
  userAvatar(): string {
    return `https://avatars.dicebear.com/api/bottts/${this.getUsername()}.svg?background=%230000ff`;
  }

}
