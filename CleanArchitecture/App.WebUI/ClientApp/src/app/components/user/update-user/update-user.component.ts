import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth-service/auth-service.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  updateUserInfoForm: any;
  submitted = false;

  // options
  gender = [
    {value : 1, option: 'Male'},
    {value : 2, option: 'Female'}
  ];

  userInfoDetails : any;

  constructor(private _formBuilder : FormBuilder,
               private _authService: AuthService) { }

  ngOnInit(): void {
    this.updateUserInfoForm = this._formBuilder.group({
      fullName : [null, Validators.required],
      email: [null, Validators.required],
      phoneNumber: [null, Validators.required],
      address: [null, Validators.required],
      dateOfBirth: [null,Validators.required]
    });

    this.updateUserInfoForm.patchValue(this._authService.userInfo);
  }

  getUserInfo(){
    this.userInfoDetails = this._authService.userInfo;
  }

  get getFormControl(){
    return this.updateUserInfoForm.controls;
   }

   onSubmitUpdateUser() : void{
    this.submitted = true;
    if(this.updateUserInfoForm.invalid) return;

   }
}
