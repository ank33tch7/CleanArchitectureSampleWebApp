import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth-guard/auth-guard.component';
import { CategoryListComponent } from './components/category/category-list/category-list.component';
import { ProductComponent } from './components/product/product.component';
import { HomeComponent } from './components/shared/home/home.component';
import { LoginComponent } from './components/user/login/login.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';

const routes: Routes = [
  { path: "", redirectTo: "/", pathMatch: "full" },
  { path: "login", component: LoginComponent, data: { title: "Login" } },
  { path: "", component: HomeComponent, canActivate: [AuthGuard] ,data: { title: "Home" } },
  { path: "product-list", component: ProductComponent, canActivate: [AuthGuard] ,data: { title: "Product List" } },
  { path: "category-list", component: CategoryListComponent, canActivate: [AuthGuard] ,data: { title: "Product List" } },
  { path: "user-profile", component: UserProfileComponent, canActivate: [AuthGuard] ,data: { title: "User Profile" } },

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
