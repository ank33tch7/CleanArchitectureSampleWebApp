﻿using App.Domain.Common;

namespace App.Domain.Entities
{
    public class ProductCategory : AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
