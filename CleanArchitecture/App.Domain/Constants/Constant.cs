﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Constants
{
    public static class Constant
    {
        public const string UserName = "username";
        public const string UserType = "usertype";
        public const string PhoneNumber = "phoneNumber";
        public const string FullName = "fullName";
        public const string CustomerId = "customerId";
        public const string Address = "address";
    }
}
